var DANBLISS = DANBLISS || {};

DANBLISS.Rational = function (n, d) {
	"use strict";
	this.n = n;
	this.d = d;
	this.toString = function() {
		"use strict";
		return this.n + "/" + this.d;
	};
	this.compare = function (a, b) {
	};
};

DANBLISS.rationalCompare = function(a,b) {
		"use strict";
		var comparison = a.n*b.d - b.n*a.d;
		return comparison;
};

DANBLISS.listAllRationals = function ( maxDenom ) {
	"use strict";
	var list=[];
	
	for(var denom=2; denom<=maxDenom; denom++) {
		for(var num=1; num<denom; num++) {
			list[list.length] = new DANBLISS.Rational(num, denom);
		}
	}

	list.sort(DANBLISS.rationalCompare);
	
	return list;
};

DANBLISS.rationalTable = function ( maxDenom ) {
	"use strict";
	var ratList = DANBLISS.listAllRationals(maxDenom);
	
	var currentNumerators = []; // currentNumerators[somedenom-1]
	for(var i=0; i<maxDenom; i++) {
		currentNumerators[i] = 0;
	}
	var table = [];
	var values = [];
	
	var lastRat = new DANBLISS.Rational(0,1);
	
	for(var j=0; j<ratList.length; j++) {
		var thisRat = ratList[j];
		if(DANBLISS.rationalCompare(lastRat, thisRat) !== 0) {
			table[table.length] = currentNumerators.slice(0);
			values[values.length] = lastRat.n / lastRat.d;
		}
		currentNumerators[thisRat.d - 1] = thisRat.n;
		lastRat = thisRat;
	}
	table[table.length] = currentNumerators.slice(0);
	values[values.length] = lastRat.n / lastRat.d;
	values[values.length] = 1;
	
	return {table: table, values: values};
}


DANBLISS.RationalGameBoard = function( maxDenom ) {
	"use strict";
	this.maxDenom = maxDenom;
	var rtable = DANBLISS.rationalTable( maxDenom );;
	this.rationalTable = rtable.table; // column (0 based) by rowDenom-1
	this.rationalValues = rtable.values;
	this.rationalMiddleValues = [];
	this.columns = this.rationalTable.length;
	this.clicks = []; // one per row (rowDenom-1)
	this.firstCols = []; // one per row.  Each entry is a list of first column ids of the blocks on that row.  Each list should end with columns.
	this.hits = []; // 0=unhit, -n = has been hit n times, n = hit once by click n.
	this.cellsRemaining = (maxDenom*(maxDenom+1))/2;
	this.cellsColliding = 0;
	this.init = function() {
		"use strict";
		var i,j;
		for(i=1; i<=this.maxDenom; i++) {
			this.clicks[i-1]=-1;
		}
		for(i=1; i<=this.maxDenom; i++) {
			this.firstCols[i-1] = [];
			this.hits[i-1] = [];
			var lastRat = -1;
			for(j=0; j<this.columns; j++) {
				if( this.rationalTable[j][i-1] !== lastRat ) {
					lastRat = this.rationalTable[j][i-1];
					this.firstCols[i-1][lastRat] = j;
					this.hits[i-1][lastRat] = 0;
				}
			}
			this.firstCols[i-1][lastRat+1] = this.columns; 
		}
		for(i=0; i<this.rationalValues.length-1; i++) {
			this.rationalMiddleValues[i] = (this.rationalValues[i] + this.rationalValues[i+1])/2;
		}
	};
	this.addClick = function( row, column ) {
		"use strict";
		if(this.clicks[row-1] !== -1) {
			throw "Cannot overwrite click";
		}
		for(var i=row; i<=maxDenom; i++) {
			var numer = this.rationalTable[column][i-1];
			var previousHits = this.hits[i-1][numer];
			if( previousHits === 0 ) {
				this.hits[i-1][numer] = row;
				this.cellsRemaining--;
			} else if(previousHits > 0) {
				this.hits[i-1][numer] = -2;
				this.cellsColliding++;
			} else {
				this.hits[i-1][numer] = previousHits-1;
			}
		}
		this.clicks[row-1] = column;
	};
	this.deleteClick = function( row ) {
		"use strict";
		if(this.clicks[row-1] === -1) {
			throw "Cannot delete nonexistent click";
		}
		var column = this.clicks[row-1];
		this.clicks[row-1] = -1;
		for(var i=row; i<=maxDenom; i++) {
			var numer = this.rationalTable[column][i-1];
			var previousHits = this.hits[i-1][numer];
			if( previousHits === 0 ) {
				throw "Expected to find a hit";
			} else if(previousHits > 0) {
				this.hits[i-1][numer] = 0;
				this.cellsRemaining++;
			} else if(previousHits === -2) {
				var leftSide = this.firstCols[i-1][numer];
				var rightSide = this.firstCols[i-1][numer+1];
				for( var j=1; j<=i; j++ ) {
					if( this.clicks[j-1]>=leftSide &&
						this.clicks[j-1]<rightSide) {
						if( this.hits[i-1][numer] < 0 ) {
							this.hits[i-1][numer] = j;
							this.cellsColliding--;
						} else {
							throw "Expected only to find one hit";
						}
						if(this.hits[i-1][numer] < 0) {
							throw "Collision should have been resolved";
						}
					}
				}
			} else {
				this.hits[i-1][numer] = previousHits+1;
			}
		}
		return column;
	};
	this.click = function( row, column ) {
		"use strict";
		if(this.clicks[row-1]===column) {
			// delete click
			return this.deleteClick(row);
		} else {
//			var columnCollision = 0;
//			for( var i=1; i<=this.maxDenom && !columnCollision; i++ ) {
//				if(this.clicks[i-1]===column) {
//					columnCollision = i;
//				}
//			}
			if(this.clicks[row-1] === -1) {
				this.addClick(row, column);
				return -1;
			} else {
				var oldCol = this.deleteClick(row);
				this.addClick(row, column);
				return oldCol;
			}
		}
	};
	this.getStatus = function() {
		if( this.cellsColliding > 0 ) {
			return -1;
		} else if( this.cellsRemaining > 0 ) {
			return 0;
		} else {
			return 1;
		}
	}
	this.init();
}

DANBLISS.RationalGameCanvas = function(maxDenom, canvas) {
	"use strict";
	this.canvas = canvas;
	this.ctx = this.canvas.getContext("2d");
	this.board = new DANBLISS.RationalGameBoard(maxDenom);
	this.gap = 2;
	this.boardLeft = 36;
	this.boardTop = 30;
	this.boardRight = 50;
	this.boardBottom = 0;
	this.status = 0;
	this.colors=[
		"#f00",
		"#bbb",
		"#55f",
		"#808",
		"#ff0",
		"#00b",
		"#0f0",
		"#080",
		
		"#f0f","#f90","#0cd","#0fa","#90d",
		"#f0f","#f90","#0cd","#0fa","#90d",
		"#f0f","#f90","#0cd","#0fa","#90d",
	];
	this.getBoardWidth = function() {
		return this.canvas.width - this.boardLeft - this.boardRight;
	}
	this.getBoardHeight = function() {
		return this.canvas.height - this.boardTop - this.boardBottom;
	}
	this.getUpperleftCornerPosition = function(row, column) {
		"use strict";
		var width = this.getBoardWidth();
		var height = this.getBoardHeight();
		return {
			x: Math.floor(((width+this.gap)*column)/this.board.columns) + this.boardLeft,
			y: Math.floor(((height+this.gap)*(row-1))/this.board.maxDenom) + this.boardTop
		}
	}
	this.getCenterPosition = function(row, column) {
		"use strict";
		var width = this.getBoardWidth();
		var height = this.getBoardHeight();
		return {
			x: Math.floor(((width+this.gap)*(column+.5))/this.board.columns) + this.boardLeft,
			y: Math.floor(((height+this.gap)*(row-.5))/this.board.maxDenom) + this.boardTop
		}
	}
	this.interperetClickCoordinates = function( x, y) {
		"use strict";
		var row = Math.floor(((y-this.boardTop)*this.board.maxDenom) / this.getBoardHeight())+1;
		var column = Math.floor(((x-this.boardLeft)*this.board.columns) / this.getBoardWidth());
		if(row<1 || row>this.canvas.maxDenom || column<0 || column >= this.board.columns) {
			return {row: -1, col: -1};
		} else {
			return {row: row, col: column};
		}
	};
	this.getBoxDimensions = function( row, numer ) {
		"use strict";
		var leftSide = this.board.firstCols[row-1][numer];
		var rightSide = this.board.firstCols[row-1][numer+1];
		var upperLeftCorner = this.getUpperleftCornerPosition(row, leftSide);
		var lowerRightCorner = this.getUpperleftCornerPosition(row+1, rightSide);
		return {
			left: upperLeftCorner.x,
			top: upperLeftCorner.y,
			width: lowerRightCorner.x - upperLeftCorner.x - this.gap,
			height: lowerRightCorner.y - upperLeftCorner.y - this.gap,
		};
	}
	this.drawCanvas = function() {
		"use strict";
		this.ctx.clearRect ( 0 , 0 , this.canvas.width, this.canvas.height );
		var i,j;
		for(i=1; i<=this.board.maxDenom; i++) {
			var rowStart = this.getUpperleftCornerPosition(i, 0);
			this.ctx.fillStyle = "#000";
			this.ctx.textBaseline = "top";
			this.ctx.font="25px Arial";
			this.ctx.fillText(""+i, 3, rowStart.y+2);
			for(j=0; j<this.board.columns; j++) {
				this.drawBox(i,j);
			}
			this.updateClickText(i);
		}
		this.writeStatus(this.status);
		this.drawClicks();
	}
	this.drawBox = function(denom, num) {
		"use strict";
		var box = this.getBoxDimensions(denom,num);
		var boxType = this.board.hits[denom-1][num];
		if(boxType===0) {
			this.ctx.fillStyle = this.colors[1];
		} else if(boxType<0) {
			this.ctx.fillStyle = this.colors[0];
		} else {
			this.ctx.fillStyle = this.colors[boxType+1];
		}
		this.ctx.fillRect(box.left, box.top, box.width, box.height);
		this.ctx.fillStyle = "#fff";
		this.ctx.fillRect(box.left, box.top+box.height, box.width, this.gap);
		this.ctx.fillRect(box.left+box.width, box.top, this.gap, box.height);
	}
	this.drawClicks = function() {
		"use strict";
		this.ctx.fillStyle = "#000000";
		for(var i=1; i<=this.board.maxDenom; i++) {
			if(this.board.clicks[i-1]>=0) {
				var clickCenter = this.getCenterPosition(i, this.board.clicks[i-1]);
				this.ctx.fillRect( clickCenter.x-4, clickCenter.y-4, 8, 8);
				this.ctx.fillRect( clickCenter.x-1, clickCenter.y, 2, this.getBoardHeight()+this.boardTop-clickCenter.y);
			}
		}
	}
	this.drawOneColumn = function(row, column) {
		"use strict";
		var i,j;
		for(i=row; i<=this.board.maxDenom; i++) {
			var num = this.board.rationalTable[column][i-1];
			this.drawBox(i,num);
		}
		this.drawClicks();
	}
	this.updateClickText = function(row) {
		"use strict";
		var ul = this.getUpperleftCornerPosition(row, this.board.columns);
		var height = this.getBoardHeight() / this.board.maxDenom;
		this.ctx.clearRect ( ul.x , ul.y , this.canvas.width - ul.x, height );
		this.ctx.fillStyle = "#000";
		this.ctx.textBaseline = "top";
		this.ctx.font="12px Arial";
		if(this.board.clicks[row-1] >= 0) {
			this.ctx.fillText(this.board.rationalMiddleValues[this.board.clicks[row-1]].toPrecision(4), 
				ul.x+2, ul.y+Math.floor((height-12)/2));
		}
	}
	this.writeStatusText = function(text, color) {
		"use strict";
		this.ctx.clearRect ( this.boardLeft , 0 , 300, this.boardTop );
		this.ctx.fillStyle = color;
		this.ctx.textBaseline = "top";
		this.ctx.font="25px Arial";
		this.ctx.fillText(text, this.boardLeft+2, 2);
	}
	this.writeStatus = function(status) {
		"use strict";
		if(status==0) {
			this.writeStatusText("","#000");
		} else if(status > 0) {
			this.writeStatusText("Completed", "#080");
		} else {
			this.writeStatusText("Collision", "#f00");
		}
	}
	this.animateVictory = function() {
		var image = this.ctx.getImageData(0,0,this.canvas.width,this.canvas.height);
		var transientImage = this.ctx.createImageData(image);
		for( var i=0; i<image.data.length; i+=4) {
			transientImage.data[i] = (image.data[i]*3+255)/4;
			transientImage.data[i+1] = (image.data[i+1]*3+255)/4;
			transientImage.data[i+2] = (image.data[i+2]*3+255)/4;
			transientImage.data[i+3] = image.data[i+3];
		}
		this.ctx.putImageData(transientImage,0,0);
		var thiz = this;
		setTimeout( function() {
			thiz.ctx.putImageData(image,0,0);
		}, 150);
	}
	this.deactivate = function() {
		this.canvas = null;
		this.ctx = null;
		$(canvas).off("click.gameevent");
	}
	this.onCanvasClick = function(event) {
		"use strict";
		var rect = this.canvas.getBoundingClientRect();
		var clickX = event.clientX-rect.left;
		var clickY = event.clientY-rect.top;
		
		var clickBox = this.interperetClickCoordinates(clickX,clickY);
		if(clickBox.row >= 0) {
			var oldColumn = this.board.click(clickBox.row, clickBox.col);
			this.drawOneColumn(clickBox.row, clickBox.col);
			if(oldColumn >= 0 && oldColumn !== clickBox.col) {
				this.drawOneColumn(clickBox.row, oldColumn);
			}
			this.updateClickText(clickBox.row);
		}
		
		var newStatus = this.board.getStatus();
		if( newStatus != this.status ) {
			this.status = newStatus;
			this.writeStatus(this.status);
			if(newStatus > 0) {
				this.animateVictory();
			}
		}
	};
	var thiz = this;
	$(canvas).on("click.gameevent",function(event){thiz.onCanvasClick(event)});
}




var gameCanvas;

$(document).ready(function() {
	"use strict";
	
	
	var optionBox = $("#rational_houses_denom");
	var canvasElement = $("#rational_houses_canvas");
	var gameDiv = $("#gamediv");
	optionBox.on("change", function(event) {
		if(gameCanvas) {
			gameCanvas.deactivate();
		}
		gameCanvas = new DANBLISS.RationalGameCanvas(Number(optionBox[0].value), canvasElement[0]);
		gameCanvas.drawCanvas();
	});
	
	gameDiv.resizable({
		stop: function(event, ui) {
			canvasElement[0].width = canvasElement[0].offsetWidth;
			canvasElement[0].height = canvasElement[0].offsetHeight;

			gameCanvas.drawCanvas();
		}
	});
	
	canvasElement[0].width = canvasElement[0].offsetWidth;
	canvasElement[0].height = canvasElement[0].offsetHeight;
	
	//optionBox[0].value="12";
	optionBox.trigger("change");
	
	
	//var gameBoard = new DANBLISS.RationalGameCanvas(17, $("#rational_houses_canvas")[0]);
	
	

});


